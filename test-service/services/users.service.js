"use strict";

const Employee = require("../models/users");


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "users",
	// version: 1

	/**
	 * Mixins
	 */
	mixins: [Employee],

	/**
	 * Actions
	 */
	actions: {
		get_docs: {
			/** @param {Context} ctx  */
			async handler(ctx) {
				const doc = await Employee.findAll();
				return doc;
			}
		},
		insert_doc: {
			/** @param {Context} ctx  */
			async handler(ctx) {
				var body = ctx.params;
				console.log({body},"---------  ")
				const doc = await Employee.create(body);
				return doc;
			}
		},
	}
};
