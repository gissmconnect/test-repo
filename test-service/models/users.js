const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize('postgres://moleculer:sakibindia@localhost:5432/moleculerdb')
const Employee = sequelize.define('Employee', {
  // Model attributes are defined here
  phoneNumber: {
    type: DataTypes.STRING(10),
    allowNull: false
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  // Other model options go here
});
Employee.sync();
module.exports = Employee;